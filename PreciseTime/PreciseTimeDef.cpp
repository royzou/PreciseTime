//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "PreciseTimeDef.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
ULONGLONG PreciseClock_us()
{
    LARGE_INTEGER Start = {0};
    QueryPerformanceCounter(&Start);

    return Start.QuadPart;
}
//---------------------------------------------------------------------------
double PreciseClockAbs_us(ULONGLONG StartTime)
{
    LARGE_INTEGER QF = {0};
    QueryPerformanceFrequency(&QF);
    LARGE_INTEGER End = {0};
    QueryPerformanceCounter(&End);
    double times = (End.QuadPart- StartTime) /(double)QF.QuadPart;  
    return times*1000*1000;
}
//---------------------------------------------------------------------------
